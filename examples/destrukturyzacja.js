
person = {
    name:'alice', 
    address:{ street:'oxford' },
    results:[120,123,42]
}

/// ======

getInfo = (person) => {
var {
    name: personName,
    address:{
        street: personLocation,
    },
    results: [,,third]
} = person
    return `${personName} from ${personLocation} - score ${third}`
}


/// ======


getInfo = ({
    name: personName,
    address:{
        street: personLocation,
    },
    company: { name:companyName } = getDefaultCompany(),
    results: [,,third]
}) => `${personName} from ${personLocation} ${companyName} - score ${third}`;


getInfo(person)