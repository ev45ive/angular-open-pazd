# GIT Clone
cd ..
git clone https://bitbucket.org/ev45ive/angular-open-pazd.git
cd angular-open-pazd
npm i 
npm start -- -o
<!-- lub -->
ng s -o

ng s --port 6666

# Zmiany
git stash -u 
git pull -f 

# Angular CLI
npm install -g @angular/cli

C:\Users\<user>\AppData\Roaming\npm\ng -> C:\Users\<user>\AppData\Roaming\npm\node_modules\@angular\cli\bin\ng

ng.cmd help
ng help

# New project
cd ..
ng new angular-open-pazdz
? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use? SCSS   [ https://sass-lang.com/documentation/syntax#scss     

# Start
cd angular-open-pazdz
ng serve -o
ng s -o

# Zoom
https://us02web.zoom.us/j/88192625432 

# Instalacje
node -v
v14.5.0
npm -v 
code -v 
git --version

# VSCode
Extenstions tab - Ctrl+Shift+X
- Angular Language Service (angular.ng-template)
- Angular 10 Snippets (mikael.angular-beastcode)
- Switcher (adrianwilczynski.switcher)
- Prettier (esbenp.prettier-vscode)
 

 # Generators / Schematics

 ng g m playlists -m app --routing 
 ng g c playlists/containers/playlists-view
 ng g c playlists/components/playlists-list
 ng g c playlists/components/playlists-list-item
 ng g c playlists/components/playlists-details
 ng g c playlists/components/playlists-form

# Bootstrap CSS
npm install bootstrap

# Pipe
ng g m shared -m playlists
ng g p shared/yesno --export true

# Core module
ng g m core -m app
ng g i core/model/playlist

# Tabs
ng g c shared/tabs --export
ng g c shared/tab-panel --export

# Emmet
https://docs.emmet.io/cheat-sheet/

# Directives
ng g d shared/tabs --export
ng g d shared/tab-toggle --export
ng g d shared/tab-panel --export


# MusicSearch

ng g m music-search -m app --routing true
ng g c music-search/containers/albums-search
ng g c music-search/components/search-form
ng g c music-search/components/search-results
ng g c music-search/components/album-card


# Service
ng g s core/services/music-search
ng g s core/services/auth

# Playlists

ng g s core/services/playlists
ng g c playlists/containers/user-playlists-view 


# Containers vs components
https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0

# BUild - fallback to index.html
https://angular.io/guide/deployment#routed-apps-must-fallback-to-indexhtml

# Ankieta

http://cutt.ly/FgtNrBb

https://www.linkedin.com/in/mateuszkulesza/



