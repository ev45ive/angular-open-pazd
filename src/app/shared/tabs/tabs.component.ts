import { Component, OnInit } from '@angular/core';
import { TabPanelComponent } from '../tab-panel/tab-panel.component';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {

  tabs: TabPanelComponent[] = []

  toggle(selected: TabPanelComponent) {
    this.tabs.forEach((tab => {
      tab.isOpen = tab == selected
    }))
  }

  unregister(tab: TabPanelComponent) {
    const index = this.tabs.indexOf(tab)
    this.tabs.splice(index, 1)
  }

  registerPanel(tab: TabPanelComponent) {
    this.tabs.push(tab)
  }

  activeTab?: TabPanelComponent

  constructor() { }

  ngOnInit(): void {
  }

}
