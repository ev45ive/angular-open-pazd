import { DurationPipe } from './duration.pipe';

describe('DurationPipe', () => {
  let pipe: DurationPipe

  beforeEach(() => {
    pipe = new DurationPipe();
  })

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });
  
  it('transforms ms to 00:00 format', () => {
    expect(pipe.transform(0)).toBe('00:00')
  })

  it('transforms ms to XX:XX format', () => {
    expect(pipe.transform(59*1000)).toBe('00:59')
    expect(pipe.transform((60+12)*1000)).toBe('01:12')
  })
});
