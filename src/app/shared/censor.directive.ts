import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, ValidationErrors } from '@angular/forms';


@Directive({
  selector: '[appCensor]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: CensorDirective, multi: true }]
})
export class CensorDirective implements Validator {

  @Input('appCensor')
  badword = ''

  validate(control: AbstractControl): ValidationErrors | null {
    const hasError = String(control.value).includes(this.badword)

    return hasError ? { 'censor': { badword: this.badword } } : null
  }
}