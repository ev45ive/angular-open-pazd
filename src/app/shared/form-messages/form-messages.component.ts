import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormGroupDirective, NgForm } from '@angular/forms';

@Component({
  selector: 'app-form-messages',
  templateUrl: './form-messages.component.html',
  styleUrls: ['./form-messages.component.scss']
})
export class FormMessagesComponent implements OnInit {

  @Input()
  field: AbstractControl | null = null

  constructor(
    // private formRef: NgForm
    public formRef: FormGroupDirective
  ) { }

  ngOnInit(): void {
  }

}
