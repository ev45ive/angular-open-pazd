import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { FormMessagesComponent } from './form-messages/form-messages.component';

@Directive({
  selector: '[appFormMessage]'
})
export class FormMessageDirective {

  @Input('appFormMessage')
  errorCode!: string

  constructor(
    private messages: FormMessagesComponent,
    private tpl: TemplateRef<Ctx>,
    private vcr: ViewContainerRef
  ) {


  }

  ngAfterViewInit(): void {
    this.messages.field!.statusChanges.subscribe(() => {
      // debugger
      const error = this.messages.field?.getError(this.errorCode)
      // console.log(error)
      if (error && !this.vcr.length) {
        this.vcr.createEmbeddedView(this.tpl, {
          $implicit: error
        })
      } else {
        this.vcr.clear()
      }
    })
  }

}

interface Ctx {
  $implicit: any
}
