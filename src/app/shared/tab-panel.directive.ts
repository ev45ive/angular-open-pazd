import { Directive, ElementRef, HostBinding, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { TabsDirective } from './tabs.directive';

@Directive({
  selector: '[appTabPanel]',
  // host: { '[hidden]': 'closed == true' }
})
export class TabPanelDirective {

  open = false

  toggle() {
    
    if(this.open){
      this.vcr.clear()
    }else{
      this.vcr.createEmbeddedView(this.tpl, {
        $implicit: Date()
      }, 0)
    }
    
    this.open = !this.open
  }

  @Input('appTabPanel')
  name = ''


  constructor(
    private tpl: TemplateRef<PanelTplContext>,
    private vcr: ViewContainerRef,
    private parent: TabsDirective) {

  }

  ngOnInit() {
    this.parent.tabs[this.name] = this
  }

}

interface PanelTplContext {
  $implicit: string
}