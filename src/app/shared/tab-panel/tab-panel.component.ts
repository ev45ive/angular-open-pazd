import { Component, ElementRef, Input, OnInit, Optional } from '@angular/core';
import { TabsComponent } from '../tabs/tabs.component';

@Component({
  selector: 'app-tab-panel',
  templateUrl: './tab-panel.component.html',
  styleUrls: ['./tab-panel.component.scss']
})
export class TabPanelComponent implements OnInit {

  @Input()
  title = ''

  isOpen = false

  toggle() {
    // this.isOpen = !this.isOpen
    this.parent.toggle(this)
  }

  constructor(
    // @Optional() private parent: TabsComponent | null,
    private parent: TabsComponent ,
    private elem: ElementRef) {
      
    console.log(parent)
    parent.registerPanel(this)
  }

  ngOnInit(): void {
  }
  ngOnDestroy(): void {
    this.parent.unregister(this)
  }
}
