import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { YesnoPipe } from './yesno.pipe';
import { FormsModule } from '@angular/forms';
import { CardComponent } from './card/card.component';
import { TabsComponent } from './tabs/tabs.component';
import { TabPanelComponent } from './tab-panel/tab-panel.component';
import { TabsDirective } from './tabs.directive';
import { TabPanelDirective } from './tab-panel.directive';
import { TabToggleDirective } from './tab-toggle.directive';
import { FormMessagesComponent } from './form-messages/form-messages.component';
import { FormMessageDirective } from './form-message.directive';
import { CensorDirective } from './censor.directive';
import { DurationPipe } from './duration.pipe';


@NgModule({
  declarations: [
    YesnoPipe,
    CardComponent,
    TabsComponent,
    TabPanelComponent,
    TabsDirective,
    TabPanelDirective,
    TabToggleDirective,
    FormMessagesComponent,
    FormMessageDirective,
    CensorDirective,
    DurationPipe,
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    YesnoPipe,
    FormsModule,
    CardComponent,
    TabsComponent,
    TabPanelComponent,
    TabsDirective,
    TabPanelDirective,
    TabToggleDirective,
    FormMessagesComponent,
    FormMessageDirective,
    CensorDirective,
    DurationPipe
  ]
})
export class SharedModule { }
