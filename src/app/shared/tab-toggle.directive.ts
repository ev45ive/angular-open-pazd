import { Directive, HostListener, Input } from '@angular/core';
import { TabsDirective } from './tabs.directive';

@Directive({
  selector: '[appTabToggle]'
})
export class TabToggleDirective {

  @Input('appTabToggle')
  name = ''

  constructor(private parent: TabsDirective,) { }

  @HostListener('click', ['$event.target'])
  toggle() {
    const tab = this.parent.tabs[this.name]
    tab.toggle()
  }

  ngOnInit() {
    this.parent.toogles[this.name] = this
  }
}
