import { Directive, ElementRef, HostListener } from '@angular/core';
import { TabPanelDirective } from './tab-panel.directive';
import { TabToggleDirective } from './tab-toggle.directive';

@Directive({
  selector: '[appTabs]'
})
export class TabsDirective {

  toogles:Record<string,TabToggleDirective> = {}
  tabs:Record<string,TabPanelDirective> = {}

  constructor(private elem:ElementRef) { 
    // $(elem.nativeElement).myFavDatePicker()
    console.log('hello tabs',elem)
    // elem.nativeElement.onclick = console.log
  }

  ngOnDestroy(): void {
    // $(elem.nativeElement).myFavDatePicker().destroy() 
  }
}
