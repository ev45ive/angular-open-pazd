import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormControlDirective, FormGroup, Validators, ValidatorFn, AbstractControl, ValidationErrors, AsyncValidatorFn } from '@angular/forms';
import { combineLatest, Observable, of, pipe } from 'rxjs';
import { debounceTime, distinct, distinctUntilChanged, filter, map, withLatestFrom } from 'rxjs/operators';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {


  censor = (badword: string): ValidatorFn => (control: AbstractControl): ValidationErrors | null => {
    const hasError = String(control.value).includes(badword)

    return hasError ? { 'censor': { badword: badword } } : null
  }

  asyncCensor = (badword: string): AsyncValidatorFn => (control: AbstractControl): Observable<ValidationErrors | null> => {
    // return this.http.post('/badwordFIlter',control.value).pipe(map(res=>resp.result))

    return new Observable((subscriber) => {
      // onSubscribe
      const hasError = String(control.value).includes(badword)
      // console.log('Form Subscribed to validator')
      const handler = setTimeout(() => {
        subscriber.next(hasError ? { 'censor': { badword: badword } } : null)
        subscriber.complete()
        // console.log('Validation complete')
      }, 1000)

      // onUnsubscribe
      return () => {
        clearTimeout(handler)
        // console.log('Form Unsubscribed')
      }
    })
    //.pipe(map(...)).subscribe({next(result){...}, error(){...}, complete(){...}}).unsubscribe()
  }

  // @Input() set query(query:string){
  //   this.searchForm.get('query')?.setValue(query)
  // }

  @Input() query = 'batman'
  ngOnChanges(changes: SimpleChanges): void {
    const field = this.searchForm.get('query') as FormControl;
    field.reset(changes['query'].currentValue,{
      emitEvent:false
    })
    
    //  field.setValue(changes['query'].currentValue,{
    //    emitEvent:false,
    //   //  onlySelf:true
    //  })
  }

  searchForm = this.bob.group({
    query: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ], [
      this.asyncCensor('batman')
    ])
  })

  constructor(private bob: FormBuilder) {
    ; (window as any).form = this.searchForm
  }

  ngOnInit(): void {
    const queryField = this.searchForm.get('query');

    const statusChanges = queryField!.statusChanges
    const valueChanges = queryField!.valueChanges as Observable<string>;

    const debouceDistinct = () => <T>(obs: Observable<T>) => obs.pipe(
      debounceTime(400),
      distinctUntilChanged(),
    )
    statusChanges.pipe(
      filter(status => status === 'VALID'),
      withLatestFrom(valueChanges),
      map(([status, value], index) => value),
      debouceDistinct(),
    )
      .subscribe(this.search)
  }

  @Output() search = new EventEmitter<string>();

  sendSearch(query: string) {
    this.search.emit(query)
  }
}
