import { ChangeDetectionStrategy, Component, HostBinding, Input, OnInit } from '@angular/core';
import { Album } from 'src/app/core/model/album';

@Component({
  selector: 'app-album-card',
  templateUrl: './album-card.component.html',
  styleUrls: ['./album-card.component.scss'],
  // host:{
  //   '[class.card]':'true'
  // },
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AlbumCardComponent implements OnInit {

  @HostBinding('class.card')
  isClass = true

  @Input()
  album!: Album

  constructor() { }

  ngOnInit(): void {
  }

}
