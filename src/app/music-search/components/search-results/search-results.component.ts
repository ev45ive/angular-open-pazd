import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { AlbumView } from '../../interfaces/AlbumView';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchResultsComponent implements OnInit {

  @Input()
  results: AlbumView[] = []

  constructor() { }

  ngOnInit(): void {
  }

  render(){
    console.log('check SearchResultsComponent')
    return Date.now()
  }

}
