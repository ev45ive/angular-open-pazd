import { Directive } from '@angular/core';
import { MusicSearchService } from 'src/app/core/services/music-search.service';

@Directive({
  selector: '[appMusicProvider]',
  providers:[
    MusicSearchService
  ]
})
export class MusicProviderDirective {

  constructor() { }

}
