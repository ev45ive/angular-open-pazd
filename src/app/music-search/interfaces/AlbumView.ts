import { Album } from 'src/app/core/model/album';

// type MockAlbums = Pick<Album, 'id' | 'name' | 'images'>[]

export type AlbumView = Pick<Album, 'id' | 'name' | 'images'>;
