import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlbumDetailsComponent } from './containers/album-details/album-details.component';
import { AlbumsSearchComponent } from './containers/albums-search/albums-search.component';
import { SyncSearchComponent } from './containers/sync-search/sync-search.component';


const routes: Routes = [
  // {
  //   path: 'music',
  //   children: [
  {
    path: '',
    redirectTo: 'search', pathMatch: 'full'
  },
  {
    path: 'search',
    component: AlbumsSearchComponent
  },
  {
    path: 'albums/:album_id',
    component: AlbumDetailsComponent
  },
  {
    path: 'sync',
    component: SyncSearchComponent
  }
  // ]
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicSearchRoutingModule { }