import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicSearchRoutingModule } from './music-search-routing.module';
import { AlbumsSearchComponent } from './containers/albums-search/albums-search.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { SyncSearchComponent } from './containers/sync-search/sync-search.component';
import { MusicProviderDirective } from './directives/music-provider.directive';
import { AlbumDetailsComponent } from './containers/album-details/album-details.component';


@NgModule({
  declarations: [
    AlbumsSearchComponent, 
    SearchFormComponent, 
    SearchResultsComponent, 
    AlbumCardComponent, SyncSearchComponent, MusicProviderDirective, AlbumDetailsComponent
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    SharedModule,
    MusicSearchRoutingModule
  ]
})
export class MusicSearchModule { }
