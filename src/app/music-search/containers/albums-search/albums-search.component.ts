import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MusicSearchService } from 'src/app/core/services/music-search.service';


@Component({
  selector: 'app-albums-search',
  templateUrl: './albums-search.component.html',
  styleUrls: ['./albums-search.component.scss'],
})
export class AlbumsSearchComponent implements OnInit {
  message = ''
  queryChanges = this.service.queryChanges

  resultsChanges = this.service.resultsChanges

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicSearchService) { }

  ngOnInit() {
    // const q = this.route.snapshot.queryParamMap.get('q')

    this.route.queryParamMap.subscribe((queryParamMap) => {
      const q = queryParamMap.get('q')
      if (q) {
        this.service.searchAlbums(q)
      }
    })
  }

  searchAlbums(query: string) {

    this.router.navigate([/* '/music', 'search' */], {
      queryParams: {
        q: query
      },
      relativeTo: this.route,
      replaceUrl:true
    })
  }

}

// this.resultsChanges = this.service
//   .sendSearchRequest(query)
//   .pipe(shareReplay())