import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { iif, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { Track } from 'src/app/core/model/album';
import { MusicSearchService } from 'src/app/core/services/music-search.service';

@Component({
  selector: 'app-album-details',
  templateUrl: './album-details.component.html',
  styleUrls: ['./album-details.component.scss']
})
export class AlbumDetailsComponent implements OnInit {

  albumId = this.route.paramMap.pipe(
    map(params => params.get('album_id'))
  )

  album = this.albumId.pipe(
    // switchMap(id => iif(() => !!id, this.service.getAlbumById(id!), of(null)))
    switchMap(id => id ? this.service.getAlbumById(id) : of(null)),
    tap(() => {
      if (this.audioRef) {
        this.audioRef.nativeElement.volume = 0.5
      }
    })
  )

  tracks = this.album.pipe(map(album => album?.tracks.items))

  currentTrack?: Track

  constructor(
    private service: MusicSearchService,
    private route: ActivatedRoute
  ) { }

  @ViewChild('audioRef', { static: false })
  audioRef?: ElementRef<HTMLAudioElement>


  selectTrack(track?: Track) {
    this.currentTrack = track;
    setTimeout(() => {
      this.audioRef?.nativeElement.play()
    })
  }

  ngOnInit(): void {
  }

  reloadTracks() {
    // this.tracks = this.http.get...
  }


}
