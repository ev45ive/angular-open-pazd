import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/compiler';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { MusicSearchService } from 'src/app/core/services/music-search.service';
import { PlaylistsService } from 'src/app/core/services/playlists.service';

import { AlbumDetailsComponent } from './album-details.component';

fdescribe('AlbumDetailsComponent', () => {
  let component: AlbumDetailsComponent;
  let fixture: ComponentFixture<AlbumDetailsComponent>;
  let routerMock: jasmine.SpyObj<Router>
  let routeMock: jasmine.SpyObj<ActivatedRoute>
  let serviceMock: jasmine.SpyObj<MusicSearchService>
  let controller: HttpTestingController

  beforeEach(async () => {
    routerMock = jasmine.createSpyObj<Router>('routerMock', [])
    routeMock = jasmine.createSpyObj<ActivatedRoute>('routeMock', [])
    serviceMock = jasmine.createSpyObj<MusicSearchService>('serviceMock', [
      'getAlbumById'
    ])

    await TestBed.configureTestingModule({
      declarations: [AlbumDetailsComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [HttpClientModule, HttpClientTestingModule],
      providers: [
        { provide: Router, useValue: routerMock },
        { provide: ActivatedRoute, useValue: routeMock },
        { provide: MusicSearchService, useValue: serviceMock },
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumDetailsComponent);
    component = fixture.componentInstance;
    controller = TestBed.inject(HttpTestingController)
      ; (routeMock.paramMap as unknown as jasmine.Spy).and.returnValue(of({
        get: () => '123'
      }))
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(serviceMock.getAlbumById).toHaveBeenCalledWith('123')
  });
});
