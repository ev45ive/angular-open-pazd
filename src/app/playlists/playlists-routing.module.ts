import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlaylistsViewComponent } from './containers/playlists-view/playlists-view.component';
import { UserPlaylistsViewComponent } from './containers/user-playlists-view/user-playlists-view.component';

const routes: Routes = [
  {
    path: 'playlists',
    component: UserPlaylistsViewComponent
  },
  {
    // playlists/create/new
    path: 'playlists/:mode',
    pathMatch:'full',
    component: UserPlaylistsViewComponent
  },
  {
    // playlists/show/123/
    path: 'playlists/:mode/:playlist_id',
    pathMatch:'full',
    component: UserPlaylistsViewComponent
  },
  // {
  //   // playlists/edit/123
  //   path: 'playlists/:mode/:playlist_id',
  //   component: UserPlaylistsViewComponent
  // },
  {
    path: 'old-playlists',
    component: PlaylistsViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistsRoutingModule { }
