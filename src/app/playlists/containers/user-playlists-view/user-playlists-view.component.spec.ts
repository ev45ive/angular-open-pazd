import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPlaylistsViewComponent } from './user-playlists-view.component';

describe('UserPlaylistsViewComponent', () => {
  let component: UserPlaylistsViewComponent;
  let fixture: ComponentFixture<UserPlaylistsViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserPlaylistsViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPlaylistsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
