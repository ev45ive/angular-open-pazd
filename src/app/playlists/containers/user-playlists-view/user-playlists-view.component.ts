import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { iif, Observable, of, ReplaySubject, Subject } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { Playlist } from 'src/app/core/model/playlist';
import { PlaylistsService } from 'src/app/core/services/playlists.service';

@Component({
  selector: 'app-user-playlists-view',
  templateUrl: './user-playlists-view.component.html',
  styleUrls: ['./user-playlists-view.component.scss']
})
export class UserPlaylistsViewComponent implements OnInit {

  message = ''

  selectedId: Observable<Playlist['id'] | null> = this.route.paramMap.pipe(
    map(params => params.get('playlist_id'))
  )

  mode: Observable<string | null> = this.route.paramMap.pipe(
    map(params => params.get('mode') || 'details')
  )

  playlists?: Observable<Playlist[]>
  selected?: Observable<Playlist | null>

  loadData() {
    this.playlists = this.service.getCurrentUserPlaylists()
    this.selected = this.selectedId.pipe(
      switchMap(id =>
        id == null ? of(null) : this.service.getPlaylistById(id)
      ),
    )
  }

  ngOnInit(): void {
    this.loadData()
  }

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private service: PlaylistsService
  ) { }

  selectPlaylist(id: Playlist['id'] | null) {
    this.router.navigate(id ? ['/playlists', 'details', id] : ['playlists'])
  }

  createPlaylist() {
    this.router.navigate(['/playlists', 'create'])
  }

  editPlaylist(playlistId: Playlist['id']) {
    // this.selectedId.subscribe(id=>{
    this.router.navigate(['/playlists', 'edit', playlistId])
    // })
  }

  savePlaylistDraft(draft: Playlist) {
    this.service.savePlaylist(draft).subscribe({
      next: (resp) => {
        this.selectPlaylist(resp?.id || draft.id)
        // this.loadData()
      },
      error: (error) => {
        this.message = error.message
      }
    })
  }

  deletePlaylist(playlist_id: Playlist['id']) {
    this.service.unfollowPlaylist(playlist_id).subscribe(() => {
      this.selectPlaylist(null)
    })
  }


}

// FIX: use switchMap:
// this.playlists.subscribe(playlists=>{
//   this.selectedId.subscribe(id => {
//     this.selected = playlists.find(p => p.id == id)
//   })
// })