import { Component, OnInit } from '@angular/core';
import { Playlist } from 'src/app/core/model/playlist';

@Component({
  selector: 'app-playlists-view',
  templateUrl: './playlists-view.component.html',
  styleUrls: ['./playlists-view.component.scss']
})
export class PlaylistsViewComponent implements OnInit {

  mode: 'create' | 'edit' | 'details' = 'details'

  playlists: Playlist[] = [
    {
      id: '123',
      type: 'playlist',
      name: 'placki 123',
      public: false,
      description: 'test',
    },
    {
      id: '234',
      type: 'playlist',
      name: 'placki 234',
      public: false,
      description: 'test',
    },
    {
      id: '345',
      type: 'playlist',
      name: 'placki 345',
      public: false,
      description: 'test',
    },
  ]

  selected?: Playlist

  constructor() { }

  ngOnInit(): void { }

  select(playlist: Playlist) {
    this.selected = playlist == this.selected? undefined : playlist
  }

  // ngDoCheck(): void {
  //   if (this.selected?.id !== this.selectedId) {
  //     this.selected = this.playlists.find(p => p.id == this.selectedId)
  //   }
  // }

  createNew() {
    this.selected = {} as Playlist
    this.mode = 'create'
  }

  edit() {
    this.mode = 'edit'
  }

  closeForm() {
    this.mode = 'details'
  }

  removePlaylist(id: Playlist['id']) {
    const index = this.playlists
      .findIndex(p => p.id === id)
    if (index !== -1) {
      this.playlists.splice(index, 1)
    }
    this.selected = undefined
  }

  savePlaylist(draft: Playlist) {
    if (draft.id) {
      const index = this.playlists
        .findIndex(p => p.id === draft.id)
      if (index !== -1) {
        this.playlists.splice(index, 1, draft)
      }
    } else {
      draft.id = Date.now().toString()
      this.playlists.push(draft)
    }
    this.selected = draft
    this.mode = 'details'

  }

}
