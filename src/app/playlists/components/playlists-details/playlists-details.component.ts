import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from 'src/app/core/model/playlist';

@Component({
  selector: 'app-playlists-details',
  templateUrl: './playlists-details.component.html',
  styleUrls: ['./playlists-details.component.scss']
})
export class PlaylistsDetailsComponent implements OnInit {

  @Input()
  playlist!: Playlist

  @Output() edit = new EventEmitter();

  editClicked(){
    this.edit.emit(this.playlist.id)
  }

  constructor() { }

  ngOnInit(): void {
  }

}
