import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { YesnoPipe } from 'src/app/shared/yesno.pipe';

import { PlaylistsDetailsComponent } from './playlists-details.component';

fdescribe('PlaylistsDetailsComponent', () => {
  let component: PlaylistsDetailsComponent;
  let fixture: ComponentFixture<PlaylistsDetailsComponent>;
  let debugElement: DebugElement;

  beforeEach((/* done */) => {
    return TestBed.configureTestingModule({
      declarations: [PlaylistsDetailsComponent, YesnoPipe],
      imports: [],
      providers: [],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      // .overridePipe(YesnoPipe, {})
      .compileComponents()//.then(done)
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistsDetailsComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    component.playlist = {
      id: '123',
      type: 'playlist',
      name: 'placki 123',
      public: false,
      description: 'test',
    }
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render name', () => {
    const elem = debugElement.query(By.css('[data-test-id="playlist_name"]')).nativeElement
    expect(elem).toBeDefined()
    expect(elem.innerText).toEqual('Placki 123')
  })

  it('should emit (edit) when button clicked', () => {
    const spy = jasmine.createSpy('Edit emitter callback')

    component.edit.subscribe(spy)
    const elem = debugElement.query(By.css('[data-test-id="edit_button"]')).triggerEventHandler('click', {
      target: { value: '' }
    })
    expect(spy).toHaveBeenCalledWith('123')

  })

});


// https://github.com/ngneat/spectator#string-selector