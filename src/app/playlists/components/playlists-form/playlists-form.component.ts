import { Component, DoCheck, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { NgForm, RequiredValidator} from '@angular/forms';
import { Playlist } from 'src/app/core/model/playlist';

@Component({
  selector: 'app-playlists-form',
  templateUrl: './playlists-form.component.html',
  styleUrls: ['./playlists-form.component.scss']
})
export class PlaylistsFormComponent/*  implements OnInit, OnChanges , DoCheck, OnDestroy */ {

  // @Input()
  // set playlist(val: Playlist) {
  //   this.draft = { ...val }
  // }
  showOptions = false

  // @ViewChild('formRef', { read: NgForm })
  @ViewChild(NgForm, { read: NgForm, static: false })
  formRef?: NgForm

  @Input()
  playlist!: Playlist

  draft?: Playlist

  constructor() {
    console.log('constructor')
  }

  ngOnDestroy(): void {
    console.log('ngOnDestroy')
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('ngOnChanges', changes)
    // this.draft = { ...this.playlist }
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      console.log('ngAfterViewInit', this.formRef)
    })
  }

  ngOnInit(): void {
    console.log('ngOnInit')
    // debugger
  }

  ngDoCheck(): void {
    console.log('ngDoCheck')
  }

  /* === */

  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter<Playlist>();

  submit(form: NgForm) {
    const {
      // name: name,
      name,
      settings: {
        public: isPublic,
        description
      } = {}
    } = form.value as {
      name: Playlist['name'],
      settings: Pick<Playlist, 'public' | 'description'>
    }

    const draft = {
      // ...(form.value as Partial<Playlist>)
      ...this.playlist,
      // name:name,
      name,
      public: isPublic || false,
      description: description || '',
    }

    this.save.emit(draft)
  }

  cancelClicked() {
    this.cancel.emit()
  }
}

type DraftKeys = 'name' | 'public' | 'description'

// type ParamsMap = {
//   [k: string]: string
// }

// type PartialPlaylist = {
//   [k in DraftKeys]?: string | number
// }

// type PartialPlaylist = {
//   [k in DraftKeys]: Playlist[k]
// }

// type Partial<T> = {
//   readonly [k in keyof T]?: T[k]
// }


// interface PartialPlaylist {
//   name: string
//   public: boolean
//   description: string
// }