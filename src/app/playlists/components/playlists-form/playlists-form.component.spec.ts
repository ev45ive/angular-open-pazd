import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaylistsFormComponent } from './playlists-form.component';

describe('PlaylistsFormComponent', () => {
  let component: PlaylistsFormComponent;
  let fixture: ComponentFixture<PlaylistsFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlaylistsFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
