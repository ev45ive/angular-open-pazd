import { NgForOf, NgForOfContext } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { Playlist } from 'src/app/core/model/playlist';

NgForOf
NgForOfContext

@Component({
  selector: 'app-playlists-list',
  templateUrl: './playlists-list.component.html',
  styleUrls: ['./playlists-list.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  // inputs:['playlists:items']
})
export class PlaylistsListComponent implements OnInit {

  @Input('items')
  playlists:Playlist[] = [  ]
  
  @Input()
  selected = this.playlists[1]

  @Output()
  selectedChange = new EventEmitter()

  @Output() remove = new EventEmitter<Playlist['id']>();

  select(selected: Playlist) {
    this.selectedChange.emit(selected==this.selected?null:selected)
  }

  removeClick(playlist: Playlist){
    this.remove.emit(playlist.id)
  }

  // constructor(
  //   private parent:PlaylistsViewComponent
  // ) { 
  //   this.playlists = parent.playlists
  // }

  ngOnInit(): void {
    // setInterval(() => {
    //   this.playlists.push(this.playlists.shift())
    // }, 1000)

  }

  // getSelected() {
  //   console.log('znowu')
  //   return this.playlists.find(p => p.id == this.selectedId)
  // }
}
