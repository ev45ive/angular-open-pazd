import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaylistsRoutingModule } from './playlists-routing.module';
import { PlaylistsViewComponent } from './containers/playlists-view/playlists-view.component';
import { PlaylistsListComponent } from './components/playlists-list/playlists-list.component';
import { PlaylistsListItemComponent } from './components/playlists-list-item/playlists-list-item.component';
import { PlaylistsDetailsComponent } from './components/playlists-details/playlists-details.component';
import { PlaylistsFormComponent } from './components/playlists-form/playlists-form.component';
import { SharedModule } from '../shared/shared.module';
import { UserPlaylistsViewComponent } from './containers/user-playlists-view/user-playlists-view.component';


@NgModule({
  declarations: [
    PlaylistsViewComponent, 
    PlaylistsListComponent, 
    PlaylistsListItemComponent, 
    PlaylistsDetailsComponent, 
    PlaylistsFormComponent, UserPlaylistsViewComponent
  ],
  imports: [
    CommonModule,
    PlaylistsRoutingModule,
    SharedModule
  ],
  // exports:[
  //   PlaylistsViewComponent
  // ]
})
export class PlaylistsModule { }
