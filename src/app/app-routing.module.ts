import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/music/search',
    // redirectTo: '/music/123/search?qwe=123',
  },

  {
    path: 'music',
    // canActivate:[ AdminGuard ] // https://angular.io/api/router/CanActivate
    loadChildren: () => import('./music-search/music-search.module').then(m => m.MusicSearchModule)
  },
  {
    path: '**',
    component: PageNotFoundComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // enableTracing: true,
    // useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
