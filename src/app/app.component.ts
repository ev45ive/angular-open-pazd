import { ChangeDetectorRef, Component } from '@angular/core';
import { AuthService } from './core/services/auth.service';
import { UserService } from './core/services/user.service';

@Component({
  selector: 'app-root, app-placki',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Lubie placki';
  hide = false

  user = this.userService.getCurrentUserProfile()

  constructor(
    private auth: AuthService,
    private userService: UserService,
  ) {}

  login() {
    this.auth.authorize()
  }
  
  logout() {
    this.auth.logout()
  }
}

// console.log(AppComponent)