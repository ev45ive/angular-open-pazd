export interface Entity {
  id: string
  name: string
}

export interface Playlist extends Entity {
  type: 'playlist'
  public: boolean
  description: string
  // tracks: Array<Track>
  tracks?: Track[]
}

export interface Track extends Entity {
  type: 'track'
  duration: number
}


// export interface Playlist extends Entity {
//   extra: string
// }


// const playlist: Playlist = {
//   id: '123',
//   type: 'playlist',
//   name: 'placki',
//   public: false,
//   description: 'test',
//   extra:'test'
// }
// // playlist.tracks= []
// if(playlist.tracks){
//   playlist.tracks.length 
// }
// playlist.tracks && playlist.tracks.length 
// playlist.tracks?.length 

// let x: Track | Playlist

// switch (x.type) {
//   case 'playlist':
//     x.description
//     break;
//   case 'track':
//     x.duration
// }

// let x: string | number;

// if(typeof x === 'string'){
//   x.bold()
// }else{
//   x.toExponential()
// }

// export class Playlist{
//   id: number;
//   name: string;
//   public: boolean;
//   description: string;
// }