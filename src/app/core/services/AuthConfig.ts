

export class AuthConfig {
  auth_url!:string
  /**
   *   Required. The client ID provided to you by Spotify when you register your application.
   */
  client_id!: string;
  /**
   * Required.Set it to “token”.
   */
  response_type!: string;
  /**
   * 	Required. The URI to redirect to after the user grants / denies permission.This URI needs to be entered in the URI whitelist that you specify when you register your application.
   */
  redirect_uri!: string;
  /**
   * Optional
   */
  state?: string;
  /**
   * A space-separated list of scopes: see Using Scopes.
   */
  scopes: string[] = [];
  show_dialog?: boolean = false;
}
