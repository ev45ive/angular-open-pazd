import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private auth: AuthService) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    
    // if(request.url.includes('inny_server')){...}
    
    const authReq = request.clone({
      setHeaders: {
        Authorization: 'Bearer ' + this.auth.getToken()
      }
    })

    return next.handle(authReq).pipe(
      catchError((err, caughtSource) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            setTimeout(() => this.auth.authorize(), 1000)
          }
          return throwError(new Error(err.error?.error?.message))
        }
        return throwError(new Error('Unexpected Error'))
      })

    )
  }
}

// // Chain of Responsibility
// res = A.handle(req)
// A.next = B
// B.next = C 
// C.next = HttpHandler
