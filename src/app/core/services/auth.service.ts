import { Inject, Injectable } from '@angular/core';
import { AuthConfig } from './AuthConfig';
import { HttpParams } from '@angular/common/http'
import { BehaviorSubject } from 'rxjs';
import { filter, map, mapTo } from 'rxjs/operators';

// angular.module('core').service('AuthService',AuthService)

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private token = new BehaviorSubject<string | null>(null);

  isLoggedIn = this.token.pipe(map(token => token !== null))

  constructor(@Inject(AuthConfig) private config: AuthConfig) { }

  init() {
    if (!this.token.getValue()) {
      this.extractToken()
    }
    if (!this.token.getValue()) {
      this.authorize()
    }
  }

  logout() {
    this.token.next(null)
  }

  authorize() {
    const { auth_url, client_id, redirect_uri, response_type, scopes, show_dialog, state } = this.config

    const params = new HttpParams({
      fromObject: {
        client_id,
        redirect_uri,
        response_type,
        scope: scopes.join(' '),
        show_dialog: show_dialog ? 'true' : 'false',
        state: state || ''
      }
    })
    // window.open(`${auth_url}?${params}`,)
    window.location.href = (`${auth_url}?${params}`)
  }

  extractToken() {
    const params = new HttpParams({
      fromString: location.hash
    })
    const token = params.get('#access_token')
    if (token) {
      window.location.hash = '' // params.get('state')
      this.token.next(token)
    }
    if (this.token.getValue()) {
      window.sessionStorage.setItem('token', window.JSON.stringify(this.token.getValue()))
    } else {
      const rawToken = sessionStorage.getItem('token');
      this.token.next(rawToken && JSON.parse(rawToken))
    }
  }

  getToken() {
    return this.token.getValue()
  }
}

// placki555@gmail.com 
// pancakes123
