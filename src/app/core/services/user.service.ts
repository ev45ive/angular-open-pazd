import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { shareReplay, switchMap } from 'rxjs/operators';
import { UserProfile } from '../model/user';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private auth: AuthService,
    private http: HttpClient
  ) { }

  userProfile = this.auth.isLoggedIn.pipe(
    switchMap(loggedIn => {
      if (loggedIn) {
        return this.http.get<UserProfile>('https://api.spotify.com/v1/me')
      } else {
        return of(null)
      }
    }),
    shareReplay()
  )
  // GET https://api.spotify.com/v1/me
  getCurrentUserProfile(): Observable<UserProfile | null> {
    return this.userProfile
  }

  // return new Observable(subscriber => {
  //   this.auth.isLoggedIn.subscribe(() => {
  //     this.http.get('https://api.spotify.com/v1/me')
  //       .subscribe((result) => {
  //           subscriber.next(result)
  //       })
  //   })
  // })
}


