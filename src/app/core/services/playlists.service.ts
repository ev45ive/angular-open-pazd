import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { iif, of, throwError } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { PagingObject } from '../model/album';
import { Playlist } from '../model/playlist';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class PlaylistsService {

  constructor(
    private http: HttpClient,
    private userService: UserService) {
  }


  // GET 	/v1/me/playlists
  getCurrentUserPlaylists() {
    return this.http.get<PagingObject<Playlist>>(`https://api.spotify.com/v1/me/playlists`).pipe(
      map(resp => resp.items)
    )
  }

  // GET	/v1/playlists/{playlist_id}
  getPlaylistById(playlist_id: Playlist['id']) {
    return this.http.get<Playlist>(`https://api.spotify.com/v1/playlists/${playlist_id}`)
  }

  savePlaylist({
    id, name, public: isPublic, description
  }: Playlist) {

    const payload = {
      name, public: isPublic, description
    }

    // Update if ID or Save if new playlist
    return iif(() => id !== undefined,

      // PUT	/v1/playlists/{playlist_id}
      this.http.put<null>(`https://api.spotify.com/v1/playlists/${id}`, payload),

      // POST /v1/users/{user_id}/playlists
      this.userService.userProfile.pipe(
        switchMap(user => {
          if (user) {
            return this.http.post<Playlist>(`https://api.spotify.com/v1/users/${user.id}/playlists`, payload)
          } else {
            return throwError(new Error('Not logged in'))
          }
        })
      )
    )
  }

  unfollowPlaylist(playlist_id:Playlist['id']){
    return this.http.delete<null>(`https://api.spotify.com/v1/playlists/${playlist_id}/followers`)
  }

}


const playlistsMock: Playlist[] = [
  {
    id: '123',
    type: 'playlist',
    name: 'placki 123',
    public: false,
    description: 'test',
  },
  {
    id: '234',
    type: 'playlist',
    name: 'placki 234',
    public: false,
    description: 'test',
  },
  {
    id: '345',
    type: 'playlist',
    name: 'placki 345',
    public: false,
    description: 'test',
  },
]