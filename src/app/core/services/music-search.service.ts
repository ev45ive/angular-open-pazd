import { EventEmitter, Inject, Injectable, Optional } from '@angular/core';
import { AlbumView } from 'src/app/music-search/interfaces/AlbumView';
import { environment } from 'src/environments/environment';
import { SEARCH_API_URL } from '../tokens';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Album, AlbumsResponse, AlbumsSearchResponse } from '../model/album';
import { AuthService } from './auth.service';
import { catchError, concatAll, delay, distinctUntilChanged, exhaust, map, mergeAll, pluck, startWith, switchAll, switchMap } from 'rxjs/operators';
import { BehaviorSubject, concat, EMPTY, from, of, ReplaySubject, Subject, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MusicSearchService {
  private errorsNotifications = new Subject<Error>()

  private results = new BehaviorSubject<Album[]>(mockAlbums as Album[])
  resultsChanges = this.results.asObservable()

  private queryStart = 'batman'
  private query = new BehaviorSubject<string>(this.queryStart)
  queryChanges = this.query.asObservable()

  // NgRX - Store
  // private state = new BehaviorSubject({
  //   results: [] as Album[],
  //   query: ''
  // })
  // NgRX - Selectors
  // queryChanges = this.state.pipe(map(s => this.query), distinctUntilChanged())

  constructor(
    @Optional() @Inject(SEARCH_API_URL)
    private api_url = environment.search_api_url,
    private http: HttpClient
  ) {
    ; (window as any).subject = this.results

    this.queryChanges.pipe(
      switchMap(query => this.sendSearchRequest(query)),
    ).subscribe(this.results)


    // Refresh every .3 minutes
    // let nextTime = 0;
    // const handler = setInterval(() => {
    //   if (Date.now() > nextTime) {
    //     this.searchAlbums('batman')
    //     nextTime = Date.now() + .3 * 60 * 1000
    //   }
    // }, 1000)

  }

  sendSearchRequest(query: string) {
    return this.http.get<AlbumsSearchResponse>(this.api_url, {
      params: ({ type: 'album', query })
    }).pipe(
      map(resp => resp.albums.items),
      catchError(error => {
        this.errorsNotifications.next(error);
        return EMPTY;
      })
    );
  }

  getAlbumById(id: Album['id']) {
    return this.http.get<Album>(`https://api.spotify.com/v1/albums/${id}`).pipe(
      catchError(error => {
        this.errorsNotifications.next(error);
        return EMPTY;
      })
    );
  }

  searchAlbums(query = 'batman'): void {
    this.query.next(query)
    // this.state.next({
    //   ...this.state.getValue(),
    //   query
    // })
  }
}



export const mockAlbums: AlbumView[] = [
  {
    id: '123', name: '123', images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/400/400' }
    ]
  },
  {
    id: '234', name: '234', images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/300/300' }
    ]
  },
  {
    id: '345', name: '345', images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/500/500' }
    ]
  },
]