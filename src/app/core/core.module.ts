import { Inject, ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from 'src/environments/environment';
import { SEARCH_API_URL } from './tokens';
import { HttpClientModule, HttpClientXsrfModule, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthConfig } from './services/AuthConfig';
import { AuthService } from './services/auth.service';
import { AuthInterceptor } from './services/auth.interceptor';

@NgModule({
  declarations: [],
  imports: [
    // HttpClientXsrfModule.withOptions({
    //   cookieName:'PLACKI-TOKEN',
    //   headerName:'X-XSRF-TOKEN'
    // }),
    HttpClientXsrfModule.disable(),
    // CommonModule,
    HttpClientModule
  ],
  providers: [
    // {
    //   provide: HttpClient,
    //   useClass: MyAuthErrorHandlingMuchMoreAwesomeHttpClient
    // },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: SEARCH_API_URL,
      useValue: environment.search_api_url
    },
    {
      provide: AuthConfig,
      useValue: environment.authConfig
    }
    // {
    //   provide: MusicSearchService,
    //   useFactory(api_url: string, routes: Route[][]) {
    //     return new MusicSearchService(api_url)
    //   },
    //   deps: [SEARCH_API_URL, ROUTES]
    // },
    // {
    //   provide: AbstractMusicSearchService,
    //   useClass: SpotifyMusicSearchService,
    //   // deps: [ALTERNATIVE_SEARCH_API_URL]
    // },
    // MusicSearchService, // Always included.. ;-( 
  ]
})
export class CoreModule {

  constructor(
    private auth: AuthService, 
    @Inject(HTTP_INTERCEPTORS) interceptors: HttpInterceptor[]
    ) {

    // console.log(interceptors)
    this.auth.init()
  }

  static forRoot(config: AuthConfig): ModuleWithProviders<CoreModule> {
    return {
      ngModule: CoreModule,
      providers: [
        {
          provide: AuthConfig,
          useValue: config
        }
      ]
    }
  }
}
