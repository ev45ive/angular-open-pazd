import { BrowserModule } from '@angular/platform-browser';
import { ApplicationRef, DoBootstrap, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PlaylistsModule } from './playlists/playlists.module';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
// import { MusicSearchModule } from './music-search/music-search.module';
import { environment } from 'src/environments/environment';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    CoreModule.forRoot(environment.authConfig),
    SharedModule,
    PlaylistsModule,
    // MusicSearchModule,
    AppRoutingModule,
  ],
  providers: [
    // {
    //   provide: 'SEARCH_API_URL',
    //   useValue: environment.search_api_url
    // }
  ],
  bootstrap: [AppComponent]
})
export class AppModule /* implements DoBootstrap  */ {
  // ngDoBootstrap(appRef: ApplicationRef): void {
  //   appRef.bootstrap(AppComponent,'app-placki')
  //   appRef.bootstrap(AppComponent,'app-root')
  // }
}
