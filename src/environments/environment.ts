// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { AuthConfig } from 'src/app/core/services/AuthConfig';

export const environment = {
  production: false,
  search_api_url: 'https://api.spotify.com/v1/search',

  authConfig: {
    auth_url: 'https://accounts.spotify.com/authorize',
    client_id: 'a7230e3109f343b39d9e043363aea8e6',
    redirect_uri: 'http://localhost:4200/music/search',
    response_type: 'token',
    scopes: [
      'playlist-read-collaborative',
      'playlist-modify-private',
      'playlist-modify-public',
      'playlist-read-private',
    ],
    show_dialog: true,
    state: ''
  } as AuthConfig
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
