import { AuthConfig } from 'src/app/core/services/AuthConfig';

export const environment = {
  production: true,
  search_api_url: 'https://api.spotify.com/v1/search',

  authConfig: {
    auth_url: 'https://accounts.spotify.com/authorize',
    client_id: 'a7230e3109f343b39d9e043363aea8e6',
    redirect_uri: 'http://localhost:4200/music/search',
    response_type: 'token',
    scopes: [
      'playlist-read-collaborative',
      'playlist-modify-private',
      'playlist-modify-public',
      'playlist-read-private',
    ],
    show_dialog: true,
    state: ''
  } as AuthConfig
};
